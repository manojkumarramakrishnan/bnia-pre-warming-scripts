package commonFiles;

import java.util.ArrayList;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import bnConfig.BNConstants;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.relevantcodes.extentreports.NetworkMode;

public class ExtentRptManager 
{
	public static ExtentReports extent;
	public static ExtentTest parent,child;
	public ArrayList<String> log = new ArrayList<>();
	
	@BeforeSuite
	public static ExtentReports completeReport()
	{
	 if(extent == null)
	 {
		 String rptPath = BNConstants.repKioskFileLoc;
		 extent = new ExtentReports(rptPath, false, NetworkMode.ONLINE);
		 //extent.loadConfig(new File(BNConstants.extentRepConfigfile));
	 }
	 return extent;
	}
	
	@AfterSuite
	public void closeReporter() 
	{
		//extent.endTest(parent);
		extent.flush();
		extent.close();
	}
	
	public void ChildCreation(String name)
	{
		child = extent.startTest(name);
		parent.appendChild(child);
	}
	
	public static void EndTest()
	{
		extent.endTest(child);
	}
	
	public void Pass(String val)
	{
		child.log(LogStatus.PASS, val);
		EndTest();
	}
	
	public static void Fail(String val)
	{
		child.log(LogStatus.FAIL, val);
		EndTest();
	}
	
	public static void Exception(String val)
	{
		child.log(LogStatus.FATAL,val);
		EndTest();
	}
	
	public void Createlog(String val)
	{
		child.log(LogStatus.INFO, val);
	}
}
