package bnConfig;

import java.text.SimpleDateFormat;
import java.util.Date;

public class BNConstants 
{
	static String filename = new SimpleDateFormat("dd-MMM-yyyy HH-mm").format(new Date())+".html";
	public static String domain = "http://bninstore.skavaone.com/";
	public static String directory = "prod";
	public static String astDevice = "bninstore";
	public static String kioskDevice = "bnkiosk";
	
	//public static String plpPageNavigation = "http://bn-uat2.skavaone.com/skavastream/studio/reader/preprod/bnkiosk/productlist?navParam=";
	//public static String kioskplpPageNavigation = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bnkiosk/productlist?navParam=";
	//public static String astplpPageNavigation = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/productlist?navParam=";
	//public static String kioskeanNavigation = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bnkiosk/pdp?navParam=";
	public static String asteanNavigation = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bninstore/pdp?navParam=";
	public static String kioskeanNavigation = "http://bninstore.skavaone.com/skavastream/studio/reader/prod/bnkiosk/pdp?navParam=";
	public static String commonExcelFile = System.getProperty("user.dir")+"\\Excel File\\BNIAExcel.xls";
	public static String repKioskFileLoc = System.getProperty("user.dir")+"\\Report\\BNIA Kiosk TestReport "+filename;
	public static String repAstFileLoc = System.getProperty("user.dir")+"\\Report\\BNIA Ast TestReport "+filename;
	//public static String extentRepConfigfile = "G:\\B&N\\BarnesAndNoble\\ExtentReportConfig.xml";
}
