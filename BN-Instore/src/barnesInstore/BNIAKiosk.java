package barnesInstore;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.relevantcodes.extentreports.NetworkMode;

import bnConfig.BNConstants;
import commonFiles.BNBasicfeature;

public class BNIAKiosk 
{
  public static ExtentReports extent;
  public static ExtentTest parent,child;
  WebDriver driver;
  WebDriverWait wait;
  //BNConstants bn = new BNConstants();
  
  @Test
  public void BNIA_Menu_Category() throws Exception 
  {
	  HSSFSheet sheet = BNBasicfeature.excelsetUp("Menu Category");
	  //HSSFSheet sheet = BNBasicfeature.excelsetUp("PDP");
	  ArrayList<String> productId = new ArrayList<>();
	  ArrayList<String> log = new ArrayList<>();
	  String noPrdtFound="";
	  int bniakey = sheet.getLastRowNum();
	  //for(int i = 1;i<=2;i++)
	  for(int i = 1;i<=bniakey;i++)
	  {
		  boolean flag,nookbook = false;
		  String menuName = sheet.getRow(i).getCell(1).getStringCellValue();
		  String categoryName = sheet.getRow(i).getCell(2).getStringCellValue();
		  String subCatgegoryName = sheet.getRow(i).getCell(3).getStringCellValue();
		  String kioskURL = sheet.getRow(i).getCell(6).getStringCellValue();
		  //System.out.println(System.getProperty("user.dir"));
		  parent = extent.startTest(menuName+" ---] "+categoryName+" ---] "+subCatgegoryName);
		  wait = new WebDriverWait(driver, 60);
		  String browseCategoryURL = kioskURL.replaceAll("http://bninstore.skavaone.com/", BNConstants.domain).replaceAll("prod", BNConstants.directory).replaceAll("bnkiosk", BNConstants.kioskDevice);
		  int plpLinkValid = BNBasicfeature.linkBroken(browseCategoryURL, log);
		  if(plpLinkValid==200)
		  {
			  nookbook = patternMatch(browseCategoryURL);
			  if(nookbook==true)
			  {
				  	String[] plpURL = browseCategoryURL.split("=");
					productId.add(plpURL[1]);
					flag=false;
			  }
			  else
			  {
				  driver.get(browseCategoryURL);
				  wait.until(ExpectedConditions.attributeContains(By.xpath("//*[@id='skMob_fullPageSpinner']"), "style", "display: none;"));
				  try
				  {
					  WebElement noprdtElement = driver.findElement(By.xpath("//*[@id='noProductsFound']"));
					  noPrdtFound = noprdtElement.getText();
					  flag=true;
				  }
				  catch(Exception e)
				  {
					  List<WebElement> productListContainer = driver.findElements(By.xpath("//*[@class='skMob_productListItemOuterCont']"));
					  for(int j = 0;j<productListContainer.size();j++)
					  {
						  productId.add(productListContainer.get(j).getAttribute("prodid"));
					  }  
					  flag=false;
				  }
			  }
			  if(flag==false)
			  {
				  for(int k=0;k<productId.size();k++)
				  //for(int k=0;k<1;k++)
				  {
					 String url="";
					 if(productId.size()>0)
					 {
						 try
						 {
							 ChildCreation("The Product ID : " + productId.get(k));
							 String eanProductNavigationURL =  BNConstants.kioskeanNavigation.replaceAll("http://bninstore.skavaone.com/", BNConstants.domain).replaceAll("prod", BNConstants.directory).replaceAll("bnkiosk", BNConstants.kioskDevice)+productId.get(k);
							 url = eanProductNavigationURL;
							 HttpClient client = HttpClientBuilder.create().build();
							 HttpGet get = new HttpGet(url);
							 HttpResponse response = client.execute(get);
							 int resp = response.getStatusLine().getStatusCode();
							 if(resp==200)
							 {
								 Pass("The EAN Number : " + productId.get(k) + " is good. The Url is : "+ url +".The Returned Status Code was : " + resp);
							 }
							 else
							 {
								 Fail("The EAN Number : " + productId.get(k) + " is not good. The Url is : "+ url +". The Returned Status Code was : " + resp);
							 }
						 }
						 catch(Exception e)
						 {
							 Exception("There is something wrong with the url.The Product id was : " + productId + " and the URL was : " + url + ".The exception was : " + e.getMessage());
						 }
					 }
					 else
					 {
						Fail("Something wrong with the url : " + browseCategoryURL + " .It seems to be empty. The received alert message was : " + noPrdtFound); 
					 }
				  }
			  }
			  else
			  {
				  ChildCreation("The URL : " + browseCategoryURL + ",seems to be list empty products.");
				  Fail("Something wrong with the url : " + browseCategoryURL + " .It seems to be empty. The received error was : " + noPrdtFound);
			  }
		  }
		  else
		  {
			  ChildCreation("The URL : " + browseCategoryURL + " is broken.The status code is not the expected one.");
			  Fail("The URL : " + browseCategoryURL + " is broken.",log);
		  }
		  extent.endTest(parent);
		  productId.removeAll(productId);
	  }
  }
  @BeforeTest
  public void beforeMethod() 
  {
	 driver = BNBasicfeature.setMobileView(); 
  }

  @AfterTest
  public void afterMethod() 
  {
	  driver.close();
  }
  
  @BeforeSuite
	public static ExtentReports completeReport()
	{
	 if(extent == null)
	 {
		 String rptPath = BNConstants.repKioskFileLoc;
		 extent = new ExtentReports(rptPath, false, NetworkMode.ONLINE);
		 //extent.loadConfig(new File(BNConstants.extentRepConfigfile));
	 }
	 return extent;
	}
	
	@AfterSuite
	public void closeReporter() 
	{
		//extent.endTest(parent);
		extent.flush();
		extent.close();
	}
	
	public void ChildCreation(String name)
	{
		child = extent.startTest(name);
		parent.appendChild(child);
	}
	
	public static void EndTest()
	{
		extent.endTest(child);
	}
	
	public void Pass(String val)
	{
		child.log(LogStatus.PASS, val);
		EndTest();
	}
	
	public static void Fail(String val)
	{
		child.log(LogStatus.FAIL, val);
		EndTest();
	}
	
	public void Fail(String val,ArrayList<String> logval)
	{
		child.log(LogStatus.FAIL, val);
		for(String lval:logval)
		{
			Createlog(lval);
		}
		logval.removeAll(logval);
		EndTest();
	}
	
	public void Createlog(String val)
	{
		child.log(LogStatus.INFO, val);
	}
	
	public static void Exception(String val)
	{
		child.log(LogStatus.FATAL,val);
		EndTest();
	}
	
	public boolean patternMatch(String url)
	{
		String[] plpURL = url.split("=");
		boolean nook = plpURL[1].matches("[0-9]+");
		return nook;
	}

}
